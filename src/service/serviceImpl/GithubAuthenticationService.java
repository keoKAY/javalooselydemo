package service.serviceImpl;

import service.IAuthenticationService;

public class GithubAuthenticationService implements IAuthenticationService {
    @Override
    public void login() {
        System.out.println(" Login with Github !");
    }

    @Override
    public void signUp() {
        System.out.println("Sign Up with Github ! ");
    }
}
