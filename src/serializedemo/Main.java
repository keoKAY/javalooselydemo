package serializedemo;
import java.io.*;
import java.util.Scanner;

public class Main {
    private static void writeObjectToFile(String fileLocation ,Person object){
        try (
                ObjectOutputStream outputStream =
                        new ObjectOutputStream(
                                new FileOutputStream(fileLocation)
                        );
        ) {
            outputStream.writeObject(object);
            System.out.println("Successfully Write a file ! ");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String fileLocation = "./mydata/person.serialized";
        Person person = new Person();
        person.inputData(input);
        person.readData();
//        Worker  worker = new Worker(1001,"james","male",23,new Address(),12,34);
        writeObjectToFile(fileLocation,person);

//        try(
//                ObjectInputStream objectInputStream = new ObjectInputStream(
//                        new FileInputStream(fileLocation)
//                )
//                ){
//              Person person = (Person )objectInputStream.readObject();
//              person.readData();
//
//        }catch (IOException | ClassNotFoundException ex){
//            ex.printStackTrace();
//        }



    }
}
