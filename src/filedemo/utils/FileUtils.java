package filedemo.utils;

import java.io.*;
import java.util.Properties;

public class FileUtils {
    public static void writeTextFile(String data ,  String fileLocation, boolean isAppend) {
        try {

            File file = new File(fileLocation).getParentFile();
            if(!file.exists()){
                file.mkdir();  // create directory if the folder doesn't exist
            }
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileLocation,isAppend));
            bufferedWriter.write(data);
        System.out.println("Write File Successfully!!!!");
        bufferedWriter.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    public  static Properties loadPropertyFile(String filePath){
        try
                (BufferedReader reader = new BufferedReader(
                        new FileReader(filePath)
                ))
        {
            Properties properties = new Properties();
            properties.load(reader);
            return properties;
        }catch (IOException ex ){
            ex.printStackTrace();
            return null;
        }
    }
}
