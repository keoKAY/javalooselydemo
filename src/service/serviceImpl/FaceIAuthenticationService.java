package service.serviceImpl;

import service.IAuthenticationService;

public class FaceIAuthenticationService implements IAuthenticationService {
    @Override
    public void login() {
        System.out.println("Login with Facebook!");
    }

    @Override
    public void signUp() {
        System.out.println("SignUp with Facebook ! ");
    }
}
