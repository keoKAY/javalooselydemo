package service;

public class AuthenticationService {
    private final IAuthenticationService authService;
    // Constructor Injection = DI -> Dependency Injection
    public AuthenticationService(IAuthenticationService authService){
        this.authService = authService;
    }

    public void singUp(){
           authService.signUp();
    }
    public void login(){
        authService.login();
    }

}
