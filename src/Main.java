import service.AuthenticationService;
import service.serviceImpl.GithubAuthenticationService;

public class Main {
    public static void main(String[] args) {
        // anonymous object !
        AuthenticationService authService = new AuthenticationService(
                new GithubAuthenticationService()
        );

        authService.login();
        authService.singUp();
    }
}
