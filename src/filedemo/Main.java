package filedemo;
import filedemo.utils.FileUtils;
import java.io.*;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
//        System.out.println("Enter your message :");
//        String message = input.nextLine();
        String textFileLocation = "./mydata/myfile.txt"; // be aware of this as well
        String message = """
                <!DOCTYPE html>
                <html lang="en">
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Document</title>
                </head>
                <body>
                    <h1> Hello World </h1>
                </body>
                </html>
                """;
        FileUtils.writeTextFile(message,textFileLocation,false);
        // for reading the data
        try(
                BufferedReader bufferedReader = new BufferedReader(
                        new FileReader(textFileLocation)
                )
                ){
                String data;
                while( (data = bufferedReader.readLine())!=null){
                    System.out.println(data );
                }
        }catch (IOException ex){
            ex.printStackTrace();// to identify the error easily
        }


    }
}
